package com.kio.web;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class YongWebApplicationTests {

    @Test
    public void contextLoads() {
        ZonedDateTime date = ZonedDateTime.now();
        ZonedDateTime fromDate = date.plusDays(-date.getDayOfMonth() + 1);
        ZonedDateTime toDate = fromDate.plusMonths(2L).plusDays(-date.getDayOfMonth() + 1);
        ZonedDateTime from = fromDate;
        while (toDate.compareTo(from) >= 0) {
            String dateStr = from.format(DateTimeFormatter.ofPattern("dd--E--EE--e--EEE"));
            System.out.println(dateStr);
            from = from.plusDays(1L);
        }

    }

}

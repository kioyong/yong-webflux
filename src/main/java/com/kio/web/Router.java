package com.kio.web;

import com.kio.web.handlers.ProjectHandler;
import com.kio.web.handlers.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class Router {

    @Bean
    public RouterFunction<ServerResponse> routerFunction(UserHandler userHandler, ProjectHandler projectHandler) {
        return route()
            .path("/api", b1 -> b1
                .path("/user", b2 -> b2
                    .GET("", userHandler::findUserBaseInfo)
                    .POST("", userHandler::handleRequest)
                    .POST("/common", userHandler::saveCommonPlan)
                    .GET("/plan", userHandler::findPlanByOpenid)
                    .GET("/plan/common", userHandler::findCommonPlan)
                    .GET("/plan/all", userHandler::findPlanByProjectName)
                ).path("/project", b2 -> b2
                    .GET("", projectHandler::findAllActiveProjects)
                    .GET("/leave", projectHandler::leaveProject)
                    .GET("/export", projectHandler::exportExcel)
                    .GET("/download", projectHandler::download)
                    .GET("/join/{projectName}", projectHandler::joinProject)
                    .GET("/allowJoin/{flag}", projectHandler::allowJoin)
                    .POST("", projectHandler::handleRequest)
                )
            )
            .build();
    }
}

package com.kio.web.util;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.*;

public class StyleUtil {

    private XSSFCellStyle titleStyle = null;
    private XSSFCellStyle annualStyle = null;
    private XSSFCellStyle sickStyle = null;
    private XSSFCellStyle dayOffStyle = null;
    private XSSFCellStyle weekendStyle = null;
    private XSSFCellStyle holidayStyle = null;
    private XSSFWorkbook workbook;

    public StyleUtil(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public XSSFCellStyle getSourceNameStyle() {
        XSSFCellStyle sourceNameStyle = workbook.createCellStyle();
        XSSFColor black = new XSSFColor(new Color(0, 0, 0), null);
        sourceNameStyle.setFillForegroundColor(new XSSFColor(new Color(0, 118, 192), null));
        sourceNameStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sourceNameStyle.setBottomBorderColor(black);
        sourceNameStyle.setRightBorderColor(black);
        sourceNameStyle.setBorderBottom(BorderStyle.THIN);
        sourceNameStyle.setBorderRight(BorderStyle.THIN);
        sourceNameStyle.setAlignment(HorizontalAlignment.CENTER);
        sourceNameStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        return sourceNameStyle;
    }

    public XSSFCellStyle getTitleStyle() {
        if (this.titleStyle != null) return this.titleStyle;
        this.titleStyle = workbook.createCellStyle();
        titleStyle.setRotation((short) 90);
        titleStyle.setFillForegroundColor(new XSSFColor(new Color(0, 118, 192), null));
        setBorder(titleStyle);
        return titleStyle;
    }

    public XSSFCellStyle getTypeStyle(String type) {
        return type.equals("03") ? getAnnualStyle() : type.equals("04") ? getSickStyle() :
            type.equals("01") ? getWeekendStyle() : type.equals("02") ? getHolidayStyle() : getDayOffStyle();
    }

    public XSSFCellStyle getAnnualStyle() {
        if (this.annualStyle != null) return this.annualStyle;
        this.annualStyle = workbook.createCellStyle();
        annualStyle.setFillForegroundColor(new XSSFColor(new Color(255, 102, 102), null));
        setBorder(annualStyle);
        return annualStyle;
    }

    public XSSFCellStyle getSickStyle() {
        if (this.sickStyle != null) return this.sickStyle;
        this.sickStyle = workbook.createCellStyle();
        sickStyle.setFillForegroundColor(new XSSFColor(new Color(255, 204, 204), null));
        setBorder(sickStyle);
        return sickStyle;
    }

    public XSSFCellStyle getWeekendStyle() {
        if (this.weekendStyle != null) return this.weekendStyle;
        this.weekendStyle = workbook.createCellStyle();
        weekendStyle.setFillForegroundColor(new XSSFColor(new Color(203, 214, 224), null));
        setBorder(weekendStyle);
        return weekendStyle;
    }

    public XSSFCellStyle getDayOffStyle() {
        if (this.dayOffStyle != null) return this.dayOffStyle;
        XSSFCellStyle dayOffStyle = workbook.createCellStyle();
        dayOffStyle.setFillForegroundColor(new XSSFColor(new Color(153, 204, 204), null));
        setBorder(dayOffStyle);
        return dayOffStyle;
    }

    public XSSFCellStyle getHolidayStyle() {
        if (this.holidayStyle != null) return this.holidayStyle;
        this.holidayStyle = workbook.createCellStyle();
        holidayStyle.setFillForegroundColor(new XSSFColor(new Color(255, 255, 153), null));
        setBorder(holidayStyle);
        return holidayStyle;
    }

    private void setBorder(XSSFCellStyle style) {
        XSSFColor black = new XSSFColor(new Color(0, 0, 0), null);
        style.setBottomBorderColor(black);
        style.setRightBorderColor(black);
        style.setLeftBorderColor(black);
        style.setTopBorderColor(black);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

}

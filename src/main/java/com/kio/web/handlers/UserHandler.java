package com.kio.web.handlers;

import com.kio.web.model.Plan;
import com.kio.web.model.User;
import com.kio.web.repository.UserRepository;
import com.kio.web.validators.UserValidators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@Component
public class UserHandler extends BaseHandler<User, UserValidators> {

    protected UserHandler(UserRepository userRepository) {
        super(User.class, new UserValidators());
        this.userRepository = userRepository;
    }

    private UserRepository userRepository;


    /**
     * 根据用户id， 返回用户的leave plan基础信息
     **/
    public Mono<ServerResponse> findUserBaseInfo(final ServerRequest request) {
        return getOpenid(request)
            .flatMap(userRepository::findUserBaseInfo)
            .flatMap(this::res);
    }

    /**
     * 初次绑定微信，保存eid信息和微信用户信息
     **/
//    public Mono<ServerResponse> updateUserInfo(final ServerRequest request) {
//        return getOpenid(request).flatMap(userRepository::findById).flatMap(
//            exists -> request.bodyToMono(User.class).flatMap(
//                body -> {
//                    if (body.getUserInfo() != null) exists.setUserInfo(body.getUserInfo());
//                    if (body.getEid() != null) exists.setEid(body.getEid());
//                    return userRepository.save(exists);
//                })).then(ok().build());
//    }
    public Mono<ServerResponse> saveCommonPlan(ServerRequest request) {
        Mono<Plan> planMono = request.bodyToMono(Plan.class);
        return userRepository.findById("commonDate").flatMap(
            exist -> planMono.flatMap(
                plan -> {
                    exist.setDateItems(plan.getDateItems());
                    return userRepository.save(exist);
                })
        ).then(ok().build());
    }

    public Mono<ServerResponse> findCommonPlan(final ServerRequest request) {
        return Mono.just("commonDate").flatMap(this::findPlanByOpenid);
    }

    public Mono<ServerResponse> findPlanByOpenid(final ServerRequest request) {
        return getOpenid(request).flatMap(this::findPlanByOpenid);
    }

    private Mono<ServerResponse> findPlanByOpenid(String id) {
        return userRepository.findById(id)
            .map(user -> {
                Plan plan = new Plan();
                plan.setOpenid(user.getOpenid());
                plan.setDateItems(user.getDateItems() == null ? new ArrayList<>() : user.getDateItems());
                plan.setEid(user.getEid());
                return plan;
            })
            .flatMap(this::res);
    }

    public Mono<ServerResponse> findPlanByProjectName(final ServerRequest request) {
        return getOpenid(request).flatMap(userRepository::findProjectPlanMono).flatMap(this::res);

    }

    @Override
    protected Mono<ServerResponse> processBody(User validBody, ServerRequest request) {
        return getOpenid(request)
            .flatMap(userRepository::findById).flatMap(
                exist -> {
                    if (validBody.getDateItems() != null) exist.setDateItems(validBody.getDateItems());
                    if (validBody.getUserInfo() != null) exist.setUserInfo(validBody.getUserInfo());
                    if (validBody.getEid() != null) exist.setEid(validBody.getEid());
                    return userRepository.save(exist);
                }
            ).then(ok().build());
    }

//    @Override
//    protected Mono<ServerResponse> onValidationErrors(Errors errors, User invalidBody, final ServerRequest request) {
//        return ServerResponse.badRequest()
//            .contentType(MediaType.APPLICATION_JSON)
//            .body(Mono.just(errors.getAllErrors().get(0).getCodes()[errors.getAllErrors().get(0).getCodes().length - 1]), String.class);
//    }

}

package com.kio.web.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.security.Principal;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
public abstract class BaseHandler<T, U extends Validator> {

    private final Class<T> validationClass;

    private final U validator;

    protected BaseHandler(Class<T> clazz, U validator) {
        this.validationClass = clazz;
        this.validator = validator;
    }


    protected static Mono<String> getOpenid(ServerRequest request) {
        return request.principal().map(Principal::getName);
    }

//    public static Mono<ServerResponse> error(Throwable error) {
//        return badRequest().body(Mono.just(new ErrorResponse(error.getMessage())), ErrorResponse.class);
//    }

    protected Mono<ServerResponse> res(Object obj) {
        return ok().body(fromObject(obj));
    }

    protected Mono<ServerResponse> monoRes(Mono<T> mono) {
        return ok().body(mono, validationClass);
    }

    abstract protected Mono<ServerResponse> processBody(T validBody, final ServerRequest request);

    public final Mono<ServerResponse> handleRequest(final ServerRequest request) {
        return request.bodyToMono(this.validationClass)
            .flatMap(body -> {
                Errors errors = new BeanPropertyBindingResult(body, this.validationClass.getName());
                this.validator.validate(body, errors);

                if (errors == null || errors.getAllErrors()
                    .isEmpty()) {
                    return processBody(body, request);
                } else {
                    return onValidationErrors(errors, body, request);
                }
            });
    }

//    protected Mono<ServerResponse> onValidationErrors(Errors errors, T invalidBody, final ServerRequest request) {
//        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errors.getAllErrors()
//            .toString());
//    }

    protected Mono<ServerResponse> onValidationErrors(Errors errors, T invalidBody, final ServerRequest request) {
        return ServerResponse.badRequest()
            .body(Mono.just(errors.getAllErrors().get(0).getCodes()[errors.getAllErrors().get(0).getCodes().length - 1]), String.class);
    }
}

package com.kio.web.handlers;

import com.kio.web.model.Project;
import com.kio.web.model.ProjectMemberVO;
import com.kio.web.repository.ProjectRepository;
import com.kio.web.repository.UserRepository;
import com.kio.web.util.StyleUtil;
import com.kio.web.validators.ProjectValidators;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.springframework.web.reactive.function.BodyInserters.fromResource;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@Component
public class ProjectHandler extends BaseHandler<Project, ProjectValidators> {

    private ProjectRepository projectRepository;
    private UserRepository userRepository;

    @Value("${export.path}")
    private String basePath;

    public ProjectHandler(ProjectRepository projectRepository,
                          UserRepository userRepository) {
        super(Project.class, new ProjectValidators());
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }


//    public Mono<ServerResponse> createProject(final ServerRequest request) {
//        return request.bodyToMono(Project.class)
//            .flatMap(service::createProject)
//            .flatMap(this::res);
//    }

    public Mono<ServerResponse> joinProject(final ServerRequest request) {
        String projectName = request.pathVariable("projectName");
        return getOpenid(request).flatMap(userRepository::findByIdAndHasProject)
            .flatMap(
                user -> projectRepository.findByProjectName(projectName)
                    .flatMap(
                        project -> {
                            project.setSize(project.getSize() + 1);
                            user.setProjectName(projectName);
                            user.setHasProject(true);
                            user.setJoinProjectDate(new Date());
                            return projectRepository.save(project).zipWith(userRepository.save(user), (p, u) -> "ok");
                        }
                    ).switchIfEmpty(Mono.error(new IllegalArgumentException("项目不存在")))
            ).switchIfEmpty(Mono.error(new IllegalArgumentException("用户不存在或者已经加入了团队")))
            .then(ok().build());
    }

    public Mono<ServerResponse> allowJoin(final ServerRequest request) {
        return getOpenid(request).flatMap(userRepository::findById)
            .flatMap(
                user -> projectRepository.findByProjectName(user.getProjectName()).flatMap(
                    project -> {
                        project.setAllowJoin(request.pathVariable("flag").equals("true"));
                        return projectRepository.save(project);
                    })
            ).then(ok().build());

    }

    public Mono<ServerResponse> findAllActiveProjects(final ServerRequest request) {
        return ok().body(projectRepository.findByActive(true), Project.class);
    }

    public Mono<ServerResponse> leaveProject(final ServerRequest request) {
        return getOpenid(request).flatMap(userRepository::findById).flatMap(
            user -> projectRepository.findByProjectName(user.getProjectName())
                .flatMap(
                    project -> {
                        if (project.getSize() == 1) project.setActive(false);
                        project.setSize(project.getSize() - 1);
                        user.setProjectName(null);
                        user.setHasProject(false);
                        user.setJoinProjectDate(null);
                        return projectRepository.save(project).zipWith(userRepository.save(user), (p, u) -> "ok");
                    }
                )
        ).flatMap(this::res);
    }

    public Mono<ServerResponse> exportExcel(final ServerRequest request) {
        return getOpenid(request).map(this::export).flatMap(this::res);
    }

    public Mono<ServerResponse> download(final ServerRequest request) {
        String fileName = request.queryParam("fileName").get();
        return ok()
            .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
            .body(fromResource(new FileSystemResource(basePath + fileName)));
    }

    @Override
    protected Mono<ServerResponse> processBody(Project validBody, ServerRequest originalRequest) {
        return userRepository.findById(validBody.getOpenid()).flatMap(
            user -> {
                validBody.setActive(true);
                validBody.setAllowJoin(true);
                user.setHasProject(true);
                user.setProjectOwner(true);
                user.setProjectName(validBody.getProjectName());
                return userRepository.save(user).zipWith(projectRepository.save(validBody), (u, p) -> p);
            }
        ).flatMap(this::res);
    }

    public String export(String openid) {
        //TODO chose fromDate and toDate
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDhhmmss");
        String now = dateFormat.format(new Date());
        ZonedDateTime date = ZonedDateTime.now();
        ZonedDateTime fromDate = date.plusDays(-date.getDayOfMonth() + 1);
        ZonedDateTime toDate = fromDate.plusMonths(2L).plusDays(-date.getDayOfMonth() + 1);

        //create excel
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("LeavePlan");
        StyleUtil util = new StyleUtil(workbook);

        //set title source name style
        XSSFRow titleRow = sheet.createRow(0);
        titleRow.setHeight((short) 1000);
        XSSFCell resourceName = titleRow.createCell(0);
        resourceName.setCellValue("Resource Name");
        resourceName.setCellStyle(util.getSourceNameStyle());
        sheet.setColumnWidth(0, 31 * 256);

        //set date title and title style
        List<String> title = this.getTitle(fromDate, toDate);
        for (int i = 0; i < title.size(); i++) {
            XSSFCell cell = titleRow.createCell(i + 1);
            cell.setCellValue(title.get(i));
            XSSFCellStyle titleStyle = util.getTitleStyle();
            cell.setCellStyle(titleStyle);
            sheet.setColumnWidth(i + 1, 4 * 256);
        }

        //get user leave plan data
        List<ProjectMemberVO> list = userRepository.findProjectPlan(openid).getMembers();
        HashMap<String, String> commonMap = new HashMap<>();
        userRepository.findProjectPlan("commonDate").getMembers()
            .get(0).getDateItems().forEach(dateItem -> commonMap.put(dateItem.getDate(), dateItem.getType()));
        for (int i = 0; i < list.size(); i++) {
            XSSFRow row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(list.get(i).getEid());
            int j = 1;
            HashMap<String, String> map = new HashMap<>();
            list.get(i).getDateItems().forEach(dateItem -> map.put(dateItem.getDate(), dateItem.getType()));
            ZonedDateTime tempDate = fromDate;
            while (toDate.compareTo(tempDate) >= 0) {
                String dateStr = tempDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                if (map.containsKey(dateStr)) {
                    row.createCell(j).setCellStyle(util.getTypeStyle(map.get(dateStr)));
                } else if (commonMap.containsKey(dateStr)) {
                    row.createCell(j).setCellStyle(util.getTypeStyle(commonMap.get(dateStr)));
                }
                j++;
                tempDate = tempDate.plusDays(1L);
            }
        }
        XSSFCellStyle typeStyle = workbook.createCellStyle();
        typeStyle.setAlignment(HorizontalAlignment.RIGHT);
        XSSFCell cell;
        HashMap<String, XSSFCellStyle> map = new HashMap<>();
        map.put("年假", util.getAnnualStyle());
        map.put("公共假期", util.getHolidayStyle());
        map.put("病假", util.getSickStyle());
        map.put("调休", util.getDayOffStyle());
        map.put("周末", util.getWeekendStyle());
        int i = list.size() + 2;
        for (String type : map.keySet()) {
            XSSFRow row = sheet.createRow(i);
            cell = row.createCell(0);
            cell.setCellValue(type);
            cell.setCellStyle(typeStyle);
            row.createCell(1).setCellStyle(map.get(type));
            i++;
        }

        String exportFileName = list.get(0).getProjectName() + "_" + now + ".xlsx";

        File file = new File(basePath + exportFileName);
        try {
            FileOutputStream fileOut = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fileOut);
            workbook.write(bos);
            workbook.close();
            bos.close();
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("导出文件成功！文件导出路径：-- {}", basePath.concat(exportFileName));
        return exportFileName;
    }

    public List<String> getTitle(ZonedDateTime fromDate, ZonedDateTime toDate) {
        List<String> titles = new ArrayList<>();
        ZonedDateTime from = fromDate;
        while (toDate.compareTo(from) >= 0) {
            String dateStr = from.format(DateTimeFormatter.ofPattern("dd-MMM"));
            titles.add(dateStr);
            from = from.plusDays(1L);
        }
        return titles;
    }
}

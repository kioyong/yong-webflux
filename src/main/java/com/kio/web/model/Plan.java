package com.kio.web.model;

import lombok.Data;

import java.util.List;

@Data
public class Plan {

    private String openid;
    private String eid;
    private List<DateItem> dateItems;
}

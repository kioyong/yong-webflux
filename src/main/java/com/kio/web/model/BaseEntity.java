package com.kio.web.model;

import lombok.Data;
import org.springframework.data.annotation.Version;

@Data
public class BaseEntity {

    @Version
    public Long version;

//    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
//    @CreatedDate
//    private Date createdDate;
//
//    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
//    @LastModifiedDate
//    private Date lastModifiedDate;
//
//    @CreatedBy
//    private String createdBy;
//
//    @LastModifiedBy
//    private String lastModifiedBy;
}

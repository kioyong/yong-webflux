package com.kio.web.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class Project extends BaseEntity {

    @Id
    private String projectId;
    private String openid;
    private String company;
    private String country;
    private String owner;
    private String projectName;
    private boolean allowJoin;
    private boolean active;
    private Integer size;
    private List<ProjectMemberVO> members;

}

package com.kio.web.model;

import lombok.Data;

@Data
public class WxUserInfo {
    private String avatarUrl;
    private String country;
    private String city;
    private Integer gender;
    private String language;
    private String nickName;
    private String province;
}

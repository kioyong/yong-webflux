package com.kio.web.model;

import lombok.Data;

@Data
public class UserBaseVO {

    private boolean projectOwner;
    private String avatarUrl;
    private String nickName;
    private boolean hasProject;
    private String projectName;
    private String openid;
    private String eid;
}

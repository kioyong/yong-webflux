package com.kio.web.model;

import lombok.Data;

@Data
public class DateItem {

    private String type;
    private String date;
    //    private Date date;
    private String background;
}

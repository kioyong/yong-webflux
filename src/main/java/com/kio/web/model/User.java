package com.kio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author LiangYong
 * @createdDate 2017/10/1.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity implements UserDetails, CredentialsContainer {

    @Id
    private String openid;

    @JsonIgnore
    private String encryptSessionKey;
    @JsonIgnore
    private String sessionKey;

    private String nickName;
    private boolean hasProject;
    private boolean projectOwner;
    private String projectName;
    private Date joinProjectDate;

    private Long phoneNumber;
    private String eid;
    private WxUserInfo userInfo;
    private List<DateItem> dateItems;

    private Set<GrantedAuthority> authorities;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private List<String> articleCollections;
    private List<String> followers;
    private List<String> following;

    private String type;


    @Override
    @JsonIgnore
    public String getPassword() {
        return "{bcrypt}" + this.encryptSessionKey;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return this.openid;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void eraseCredentials() {
    }


}

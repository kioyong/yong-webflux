package com.kio.web.model;

import lombok.Data;

import java.util.List;

@Data
public class ProjectMemberVO {

    private String eid;
    private String nickName;
    private String avatarUrl;
    private String openid;
    private String projectName;
    private List<DateItem> dateItems;
}

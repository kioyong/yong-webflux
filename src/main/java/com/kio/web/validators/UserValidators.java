package com.kio.web.validators;

import com.kio.web.model.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UserValidators implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "openid", "异常操作,openid为空");
        User request = (User) target;
        if ((request.getEid() == null || request.getEid().isEmpty()) && (request.getDateItems() == null)) {
            errors.reject("昵称不能为空");
        }
    }
}
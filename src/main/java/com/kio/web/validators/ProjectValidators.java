package com.kio.web.validators;

import com.kio.web.model.Project;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ProjectValidators implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Project.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "openid", "openid can not be null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "projectName", "projectName can not be null");
    }
}

package com.kio.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;


@EnableWebFlux
@SpringBootApplication
public class YongWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(YongWebApplication.class, args);
    }

}

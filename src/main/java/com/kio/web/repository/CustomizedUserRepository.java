package com.kio.web.repository;

import com.kio.web.model.Project;
import com.kio.web.model.ProjectMemberVO;
import com.kio.web.model.UserBaseVO;
import reactor.core.publisher.Mono;

import java.util.List;

public interface CustomizedUserRepository {
    Mono<UserBaseVO> findUserBaseInfo(String id);

    Mono<Project> findProjectPlanMono(String openid);

    Project findProjectPlan(String openid);

}

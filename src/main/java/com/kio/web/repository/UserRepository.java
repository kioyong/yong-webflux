package com.kio.web.repository;

import com.kio.web.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<User, String>, CustomizedUserRepository {

    @Query("{ $and : [{ '_id' : ?0 },{ $or : [{ 'hasProject' : false },{ 'hasProject' : { $exists : false }}]}]}")
    Mono<User> findByIdAndHasProject(String id);
}

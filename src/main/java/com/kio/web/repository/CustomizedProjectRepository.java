package com.kio.web.repository;

import com.kio.web.model.Project;
import reactor.core.publisher.Mono;

public interface CustomizedProjectRepository {

    Mono<String> updateSizeByProjectName(String projectName);
}

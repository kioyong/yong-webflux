package com.kio.web.repository;

import com.kio.web.model.Project;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProjectRepository extends ReactiveMongoRepository<Project, String>,CustomizedProjectRepository {

    Mono<Project> findByProjectName(String projectName);
    Flux<Project> findByActive(boolean isActive);
}

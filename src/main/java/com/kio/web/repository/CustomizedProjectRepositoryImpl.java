package com.kio.web.repository;

import com.kio.web.model.Project;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class CustomizedProjectRepositoryImpl implements CustomizedProjectRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public Mono<String> updateSizeByProjectName(String projectName) {
        Update update = new Update().inc("size", 1);
        Query query = new Query(Criteria.where("projectName").is(projectName));
        Project project = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Project.class);
        if (project == null || project.getProjectName() == null)
            return Mono.error(new IllegalArgumentException("project not exist!"));
        return Mono.just("ok");
    }
}

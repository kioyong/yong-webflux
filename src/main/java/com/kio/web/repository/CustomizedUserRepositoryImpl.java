package com.kio.web.repository;

import com.kio.web.model.Project;
import com.kio.web.model.User;
import com.kio.web.model.UserBaseVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Objects;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.ConditionalOperators.IfNull.ifNull;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Slf4j
@Repository
@AllArgsConstructor
public class CustomizedUserRepositoryImpl implements CustomizedUserRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public Mono<UserBaseVO> findUserBaseInfo(String id) {
        Aggregation agg = newAggregation(User.class,
            match(where("_id").is(id)),
            project("hasProject", "projectName", "projectOwner", "openid", "eid")
                .and("userInfo.nickName").as("nickName")
                .and("userInfo.avatarUrl").as("avatarUrl")
        );
        AggregationResults<UserBaseVO> result = mongoTemplate.aggregate(agg, "user", UserBaseVO.class);
        return Mono.just(Objects.requireNonNull(result.getUniqueMappedResult()));
//        return Mono.error(new IllegalAccessException("this is test error"));
    }

    @Override
    public Mono<Project> findProjectPlanMono(String openid) {
        return Mono.just(findProjectPlan(openid)).map(project -> {
            project.getMembers().forEach( member -> {
                    if (member.getEid() != null && member.getEid().length() > 10) {
                        member.setEid(member.getEid().substring(0, 8).concat("..."));
                    }
                    if (member.getEid() == null) member.setEid("");
                });
            return project;
        });

    }

    @Override
    public Project findProjectPlan(String openid) {
        User user = mongoTemplate.findOne(query(where("_id").is(openid)), User.class);
        if (user.isHasProject() && !user.getProjectName().isEmpty()) {
            Aggregation agg = newAggregation(User.class,
                match(where("projectName").is(user.getProjectName()).and("eid").exists(true)),
                project("projectName", "openid", "eid")
                    .and(ifNull("dateItems").then(new ArrayList<>())).as("dateItems")
                    .and("userInfo.avatarUrl").as("avatarUrl")
                    .and("userInfo.nickName").as("nickName"),
                group().push("$$ROOT").as("members").first("projectName").as("projectName"),
                lookup("project", "projectName", "projectName", "project"),
                unwind("project"),
                project("members")
                    .and("project._id").as("_id")
                    .and("project.openid").as("openid")
                    .and("project.company").as("company")
                    .and("project.country").as("country")
                    .and("project.owner").as("owner")
                    .and("project.projectName").as("projectName")
                    .and("project.allowJoin").as("allowJoin")
                    .and("project.active").as("active")
                    .and("project.size").as("size")
            );
            AggregationResults<Project> result = mongoTemplate.aggregate(agg, "user", Project.class);
            return result.getUniqueMappedResult();
        }
        return null;
    }
}
